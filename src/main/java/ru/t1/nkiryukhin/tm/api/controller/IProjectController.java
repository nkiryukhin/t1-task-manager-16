package ru.t1.nkiryukhin.tm.api.controller;

import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;

public interface IProjectController {

    void changeProjectStatusById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void clearProjects();

    void completeProjectById() throws AbstractException;

    void completeProjectByIndex() throws AbstractException;

    void createProject() throws AbstractFieldException;

    void removeProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void showProjectById() throws AbstractFieldException;

    void showProjectByIndex() throws AbstractFieldException;

    void showProjects();

    void startProjectById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

}
